from django.utils import timezone
from django.db import models


# Create your models here.
class Dj_user(models.Model):
    date = models.DateTimeField(default=timezone.now, db_index=True)


class ProstoUser(models.Model):
    date = models.DateTimeField(default=timezone.now, db_index=True)
